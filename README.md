# Newsroom Frontend

Angular frontend for article listing application.
* Has frontend article listing layout and basic login form
* News articles are staic now, the json is generated from http://newsapi.org/v2/everything?domains=indianexpress.com&apiKey=2b331a56d424433f99480aaf8b3021ab
* Laravel will fetch the articles from the API and generate the JSON for angular app [Pending]
* Laravel will provide API for members login [Pending]


## Development server
* Run `npm install` to install dependancies.
* Run `npm run dev:ssr` for a dev server. Navigate to `http://localhost:4200/`.