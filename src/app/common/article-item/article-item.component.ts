import { Component, OnInit, Input } from '@angular/core';
import { NewsArticle } from '../../models/newsarticle';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.css']
})
export class ArticleItemComponent implements OnInit {

  @Input() newsarticle: NewsArticle;

  constructor() { this.newsarticle = new NewsArticle; }

  ngOnInit(): void {
  }

}
