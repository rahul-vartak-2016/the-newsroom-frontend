import { Component, OnInit } from '@angular/core';
import { NewsArticle } from '../../models/newsarticle';

@Component({
  selector: 'app-news-articles',
  templateUrl: './news-articles.component.html',
  styleUrls: ['./news-articles.component.css']
})

export class NewsArticlesComponent implements OnInit {

  newsarticles: NewsArticle[];

  constructor() { this.newsarticles = []; }

  ngOnInit(): void {

    //Working with static data for now
     this.newsarticles = [
      {
        author: "Entertainment Desk",
        title: "Actor Soumitra Chatterjee passes away at 85",
        description: "In a career spanning six decades, Soumitra Chatterjee worked in over 200 films along with doing television shows and plays.",
        url: "https://indianexpress.com/article/entertainment/regional/actor-soumitra-chatterjee-passes-away-6719929/",
        urlToImage: "https://images.indianexpress.com/2020/10/page-2.jpg",
        publishedAt: "2020-11-15T07:02:52Z",
        content: "Soumitra Chatterjee was 85. (Photo: Express Archive)Actor Soumitra Chatterjee passed away on Sunday in Kolkata following COVID-19 complications. He was 85.\r\nAn official statement from the hospital re… [+1871 chars]"
      },
      {
        author: "Express Web Desk",
        title: "Kerala Lottery Pooja Bumper BR-76 Today Results: First prize is for worth Rs 5 crore!",
        description: "Kerala Pooja Bumper BR-76 Lottery Today Result LIVE 15.11.2020, Kerala Lottery Today Results 30.11.2020: The second prize of Rs 50 lakh will be given to 10 tickets - with Rs 10 lakh for each ticket. 10 ticket holders will get Rs 5 lakh each as third prize.",
        url: "https://indianexpress.com/article/india/kerala-lottery-pooja-bumper-br-76-today-results-7052200/",
        urlToImage: "https://images.indianexpress.com/2020/11/IMG_3374.jpg",
        publishedAt: "2020-11-15T07:00:53Z",
        content: "Kerala Pooja Bumper BR-76 Lottery Result: With seven daily and multiple bumpers draws, the lottery is one of the biggest cash flows in Kerala.\r\nKerala Pooja Bumper Lottery BR-76 Today Results: The Ke… [+1519 chars]"
      },
      {
        author: "PTI",
        title: "Bowling at India’s world class batting unit will be a test for me: Leggie Mitchell Swepson",
        description: "Included in the Australia A squad for the two warm-up games against India, Swepson is now hoping to get a chance to bowl to Kohli and company.",
        url: "https://indianexpress.com/article/sports/cricket/bowling-at-indias-world-class-batting-unit-will-be-a-test-for-me-leggie-mitchell-swepson-7052243/",
        urlToImage: "https://images.indianexpress.com/2017/03/mitchell-swepson-fb.jpg",
        publishedAt: "2020-11-15T06:50:37Z",
        content: "By: PTI | \r\nUpdated: November 15, 2020 12:22:30 pm\r\nSwepson took 23 wickets in three Marsh Sheffield Shield matches this season at an average of 21.17 to present a strong case for himself but says he… [+2243 chars]"
      },
      {
        author: "IE Online",
        title: "Trump waves to supporters as thousands rally in Washington",
        description: "Even if short on numbers, the crowd was not lacking in enthusiasm for the president or outrage over the grievances he has raised over the past four years.",
        url: "https://indianexpress.com/article/world/trump-waves-to-supporters-as-thousands-rally-in-washington-7052229/",
        urlToImage: "https://images.indianexpress.com/2020/11/Trump-22.jpg",
        publishedAt: "2020-11-15T06:45:43Z",
        content: "November 15, 2020 12:15:43 pm\r\nSupporters of President Donald Trump rally in support of the president, near Freedom Plaza in Washington on Saturday, Nov. 14, 2020. (Kenny Holston/The New York Times)\r… [+5699 chars]"
      },
      {
        author: "Nithya Pandian",
        title: "Hope rises from the dead as red-headed vultures are spotted again in Nilgiris",
        description: "The sighting of a juvenile red-headed vulture bodes well for the species, whose population has dwindled to critically endangered due to poisoning of animal carcasses by locals and nesting challenges.",
        url: "https://indianexpress.com/article/india/hope-rises-from-the-dead-as-red-headed-vultures-are-spotted-again-in-nilgiris-7052217/",
        urlToImage: "https://images.indianexpress.com/2020/11/featured-image.jpg",
        publishedAt: "2020-11-15T06:42:33Z",
        content: "Red-headed vulture pair, image courtesy Dr Jean-Philippe Puyravad (left) and red-headed vulture juvenile, image courtesy Santhana Raman (right)Please dont mention the exact place where I saw them. If… [+4649 chars]"
      },
      {
        author: "Sports Desk",
        title: "Shahid Afridi dons unusually-designed helmet during PSL 2020 playoffs",
        description: "Shahid Afridi, upon his PSL return, donned a dangerous-looking helmet, which had the top bar of the grille removed.",
        url: "https://indianexpress.com/article/sports/cricket/shahid-afridi-helmet-multan-sultans-psl-2020-playoffs-7052175/",
        urlToImage: "https://images.indianexpress.com/2020/11/afridi-helmet.jpg",
        publishedAt: "2020-11-15T06:42:10Z",
        content: "Shahid Afridi plays for Multan Sultans in PSL 2020. Shahid Afridi made his comeback to competitive cricket with a new-look helmet on Saturday in the opening Pakistan Super League (PSL) playoff match … [+2562 chars]"
      },
      {
        author: "Ralph Alex Arakal",
        title: "Explained: New quarantine rules for international passengers in Karnataka",
        description: "As per the latest quarantine protocol issued by the Health Commissioner, all international passengers entering the state will have to undergo a 14-day home quarantine. However, they can be exempted from the same if they are asymptomatic",
        url: "https://indianexpress.com/article/explained/quarantine-rules-for-international-passengers-in-karnataka-bengaluru-coronavirus-7052206/",
        urlToImage: "https://images.indianexpress.com/2020/11/ch1391854.jpg",
        publishedAt: "2020-11-15T06:29:13Z",
        content: "Written by Ralph Alex Arakal\r\n, Edited by Explained Desk | Bengaluru | \r\nNovember 15, 2020 11:59:13 am\r\nThose returning from other countries to the state, including at Bengaluru, can seek exemption f… [+5163 chars]"
      },
      {
        author: "Kshitij Rawat",
        title: "What to watch on November 15: The Crown season 4 is streaming on Netflix",
        description: "Here are the movies and shows you can stream on ZEE5, Netflix, Amazon Prime Video, Disney+ Hotstar, ALTBalaji, SonyLIV, Apple TV+ and Voot among others.",
        url: "https://indianexpress.com/article/entertainment/web-series/what-to-watch-on-november-15-the-crown-is-streaming-on-netflix-7052209/",
        urlToImage: "https://images.indianexpress.com/2020/11/what-to-watch-nov-15-1200.jpg",
        publishedAt: "2020-11-15T06:27:10Z",
        content: "The Crown season 4 is streaming on Netflix. (Photo: Netflix)Even the festive season this year is shadowed by Covid-19 as most people have been reluctant to step out of their homes due to the fear of … [+2957 chars]"
      },
      {
        author: "AP",
        title: "Thomas Bach comes to Tokyo as cheerleader for next year’s Olympics",
        description: "Thomas Bach is unlikely to give new details in public, but he has said repeatedly that the IOC is planning \"many scenarios\" to get 11,000 athletes into Tokyo, and some fans, too.",
        url: "https://indianexpress.com/article/sports/sport-others/thomas-bach-tokyo-visit-olympics-cheerleader-7052063/",
        urlToImage: "https://images.indianexpress.com/2018/07/ioc-fb.jpg",
        publishedAt: "2020-11-15T06:10:29Z",
        content: "By: AP | Tokyo | \r\nUpdated: November 15, 2020 11:41:34 am\r\nIOC President Thomas Bach attends a press conference. (AP Photo)The IOC and Tokyo Olympic organizers have been shouting the message for mont… [+3409 chars]"
      },
      {
        author: "Education Desk",
        title: "UPSEE third round seat allotment result released, check here",
        description: "UPSEE third round seat allotment result 2020: The candidates need to confirm their admission by November 17, and pay the requisite, seat allotment fee by November 18",
        url: "https://indianexpress.com/article/education/upsee-third-round-seat-allotment-result-released-upsee-nic-in-7052168/",
        urlToImage: "https://images.indianexpress.com/2020/11/UPSEE-1200.jpg",
        publishedAt: "2020-11-15T06:04:20Z",
        content: "Check third round allotment result at upsee.nic.in. Representational image/ file UPSEE third round seat allotment result 2020: The third round seat allotment result of UPSEE counselling has been rele… [+1356 chars]"
      },
      {
        author: "New York Times",
        title: "Biden asked Republicans to give him a chance. They’re not interested",
        description: "Before Biden is a wall of Republican resistance, starting with Trump’s refusal to concede, extending to GOP lawmakers’ reluctance to acknowledge his victory and stretching to ordinary voters who deny the election’s outcome.",
        url: "https://indianexpress.com/article/world/biden-asked-republicans-to-give-him-a-chance-theyre-not-interested-7052172/",
        urlToImage: "https://images.indianexpress.com/2020/11/Joe-Biden-1-1200.jpg",
        publishedAt: "2020-11-15T06:00:39Z",
        content: "With COVID-19 surging across the country, Biden's top health care priority is whipping the federal government's response into shape. Biden's to-do list on health care begins with new hires and a rewr… [+2287 chars]"
      },
      {
        author: "New York Times",
        title: "It took a century to open a Mosque in Athens. Then came the pandemic",
        description: "With the coronavirus spiking, though, restrictions immediately limited a building designed for around 350 worshippers to only 13 at a time. Then, the day after the mosque’s first Friday Prayer, Greece returned to a national lockdown, forcing it to shut comple…",
        url: "https://indianexpress.com/article/world/it-took-a-century-to-open-a-mosque-in-athens-then-came-the-pandemic-7052177/",
        urlToImage: "https://images.indianexpress.com/2020/11/muslims-1.jpg",
        publishedAt: "2020-11-15T05:59:39Z",
        content: "The first act of Parliament aimed at opening a mosque came in 1890, an effort that intensified in recent decades as the Muslim community has grown, now praying in dozens of unpermitted makeshift venu… [+2604 chars]"
      },
      {
        author: "IE Online",
        title: "China’s new testing policy for travelers is problematic, say experts",
        description: "It requires inbound travelers to present negative results from an antibody test — which can neither reliably rule out infections nor prove that a person is not transmitting the virus to others.",
        url: "https://indianexpress.com/article/world/chinas-new-testing-policy-for-travelers-is-problematic-say-experts-7052157/",
        urlToImage: "https://images.indianexpress.com/2020/11/AP20317298271382.jpg",
        publishedAt: "2020-11-15T05:56:22Z",
        content: "November 15, 2020 11:26:22 am\r\nA traveler wearing a face mask to protect against the coronavirus sits at a boarding gate at the Shanghai Hongqiao International Airport in Shanghai, (AP Photo/Mark Sch… [+5778 chars]"
      },
      {
        author: "Nandagopal Rajan",
        title: "Samsung Soundbar Q800T review: A premium listening experience",
        description: "Though on the more expensive side, the Samsung Q800T is clearly a premium listening experience and worth every penny you spend on it.",
        url: "https://indianexpress.com/article/technology/tech-reviews/samsung-soundbar-q800t-review-7052151/",
        urlToImage: "https://images.indianexpress.com/2020/11/samsung-soundbar-featured.jpeg",
        publishedAt: "2020-11-15T05:50:58Z",
        content: "Samsung Q800T soundbar comes with a very modern remote The first time I experienced Dolby Atmos at a special screening in Chennai, I was blown away by the experience. But then I thought of it as some… [+4632 chars]"
      },
      {
        author: "New York Times",
        title: "More than 1 in 400 Americans test positive in a week",
        description: "The seven-day average of new daily cases is more than 140,000, with upward trends in 49 states. Some 30 states added more cases in the last week than in any other seven-day period.",
        url: "https://indianexpress.com/article/world/more-than-1-in-400-americans-test-positive-in-a-week-7052051/",
        urlToImage: "https://images.indianexpress.com/2020/11/VIRUS-DEATHS-3.jpg",
        publishedAt: "2020-11-15T05:43:01Z",
        content: "Medical workers sanitize themselves before testing for the coronavirus at a mall in Omaha, Neb (Calla Kessler/The New York Times)Written by Mitch Smith, Richard Pérez-Peña, Karen Zraick and Ron DePas… [+3727 chars]"
      },
      {
        author: "Lifestyle Desk",
        title: "Mysuru Palace may soon have India’s first sandalwood museum",
        description: "According to reports, the museum is likely to be inaugurated after November 25",
        url: "https://indianexpress.com/article/lifestyle/destination-of-the-week/mysuru-palace-may-soon-have-indias-first-sandalwood-museum-7052150/",
        urlToImage: "https://images.indianexpress.com/2020/11/pixabay_mysore-palace-1200.jpg",
        publishedAt: "2020-11-15T05:39:29Z",
        content: "The museum will be shifted to the famous Mysuru Palace, which is already quite popular among tourists -- both local and those coming from outside the state. (Source: Pixabay)India is home to many mus… [+2137 chars]"
      },
      {
        author: "Kshitij Rawat",
        title: "Sylvester Stallone joins James Gunn’s DC movie The Suicide Squad",
        description: "James Gunn took to Instagram to reveal the news. The Suicide Squad is slated to be released on August 6, 2021.",
        url: "https://indianexpress.com/article/entertainment/hollywood/sylvester-stallone-joins-james-gunn-dc-movie-the-suicide-squad-7052121/",
        urlToImage: "https://images.indianexpress.com/2020/11/james-gunn-sylvester-stallone-1200.jpg",
        publishedAt: "2020-11-15T05:14:20Z",
        content: "Written by Kshitij Rawat\r\n | New Delhi | \r\nUpdated: November 15, 2020 10:45:50 am\r\nSylvester Stallone's role in The Suicide Squad is likely to be brief. (Photo: James Gunn/Instagram)The Suicide Squad… [+1358 chars]"
      },
      {
        author: "Bloomberg",
        title: "TikTok employee agrees to drop suit against Trump administration",
        description: "TikTok owner Bytedance Ltd. this week won a 15-day extension of its deadline to sell U.S. assets",
        url: "https://indianexpress.com/article/technology/tech-news-technology/tiktok-donald-trump-employee-lawsuit-dropped-7052120/",
        urlToImage: "https://images.indianexpress.com/2020/10/tik-tok-1-1-1.jpg",
        publishedAt: "2020-11-15T05:08:18Z",
        content: "By: Bloomberg | \r\nNovember 15, 2020 10:38:18 am\r\nThe TikTok emloyee has agreed to drop the case (Image: Bloomberg)A TikTok employee who sued the Trump administration over its ban of the video-sharing… [+732 chars]"
      },
      {
        author: "IE Online",
        title: "CAT 2020: Section-wise preparation tips for IIM entrance exam",
        description: "Since not many days re left for the IIM entrance test, here is a look at an action-oriented strategy to score good marks in CAT 2020.",
        url: "https://indianexpress.com/article/education/cat-2020-section-wise-preparation-tips-for-iim-entrance-exam-7050616/",
        urlToImage: "https://images.indianexpress.com/2020/11/CAT.jpg",
        publishedAt: "2020-11-15T05:04:55Z",
        content: "New Delhi | \r\nUpdated: November 15, 2020 10:35:05 am\r\nIIM 2020 will be held on November 29. (Express photo by Partha Paul/Representational)— Written by Amit Poddar\r\nThe Common Admission Test (CAT) is… [+8400 chars]"
      }
    ]; 
  }

}
