import { Component, OnInit } from '@angular/core';
import { NewsArticle } from '../../models/newsarticle';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  newsarticle: NewsArticle;

  constructor() { this.newsarticle = new NewsArticle; }

  ngOnInit(): void {
    this.newsarticle = {
      author: "Entertainment Desk",
      title: "Actor Soumitra Chatterjee passes away at 85",
      description: "In a career spanning six decades, Soumitra Chatterjee worked in over 200 films along with doing television shows and plays.",
      url: "https://indianexpress.com/article/entertainment/regional/actor-soumitra-chatterjee-passes-away-6719929/",
      urlToImage: "https://images.indianexpress.com/2020/10/page-2.jpg",
      publishedAt: "2020-11-15T07:02:52Z",
      content: "Soumitra Chatterjee was 85. (Photo: Express Archive)Actor Soumitra Chatterjee passed away on Sunday in Kolkata following COVID-19 complications. He was 85.\r\nAn official statement from the hospital re… [+1871 chars]"
    }
  }

}
