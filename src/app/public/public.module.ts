import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from '../app-routing.module';
import { NewsArticlesComponent } from '../common/news-articles/news-articles.component';
import { ArticleItemComponent } from '../common/article-item/article-item.component';
import { BannerComponent } from '../common/banner/banner.component';

@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    LoginComponent,
    NewsArticlesComponent,
    ArticleItemComponent,
    BannerComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ]
})
export class PublicModule { }
